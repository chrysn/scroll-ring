# scroll-ring and coap-scroll-ring-server

See [scroll-ring](./scroll-ring/) for the data structure,
and [coap-scroll-ring-server](./coap-scroll-ring-server/) for a way to make the consumer side of the ring buffer usable over the network.
