/// Write as much data into out as there is in in1 concatenated with in2
pub(crate) fn write_from_slices(out: &mut [u8], in1: &[u8], in2: &[u8]) {
    if in1.len() > out.len() {
        out.copy_from_slice(&in1[..out.len()])
    } else {
        let (out_head, out) = out.split_at_mut(in1.len());
        out_head.copy_from_slice(&in1);
        if in2.len() > out.len() {
            out.copy_from_slice(&in2[..out.len()])
        } else {
            out[..in2.len()].copy_from_slice(in2);
        }
    }
}

#[test]
fn test_write_from_slices() {
    let mut out = [0, 0, 0, 0];
    write_from_slices(&mut out, &[1, 2, 3], &[4, 5, 6]);
    assert_eq!(out, [1, 2, 3, 4]);
    write_from_slices(&mut out, &[], &[11, 12]);
    assert_eq!(out, [11, 12, 3, 4]);
    write_from_slices(&mut out, &[], &[]);
    assert_eq!(out, [11, 12, 3, 4]);
    write_from_slices(&mut out, &[21, 22, 23, 24, 25], &[26]);
    assert_eq!(out, [21, 22, 23, 24]);
}
