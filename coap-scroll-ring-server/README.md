# coap-scroll-ring-server

An implemenmtation of a [coap_handler::Handler] around a [scroll_ring::Buffer]

### Usage

Wrap a buffer (typically some tasks's linear text output) through
[`BufferHandler::new(&buf)`](BufferHandler::new) and place it [`.at(&["output"],
...)`](coap_handler_implementations::HandlerBuilder::at) some CoAP handler tree.

### Examples

This crate comes with no example, but the
[coap-message-demos](https://crates.io/crates/coap-message-demos) crate makes ample use of it
for its logging adapter.

### Tools

In the `tools/` directory of this crate there is a Python script, which should be useful for
speedy collection of a ring buffer server's output.

License: MIT OR Apache-2.0
